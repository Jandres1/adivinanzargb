var colores=[
  'rgb(255, 0, 0)',
  'rgb(0, 0, 255)',
  'rgb(0, 255, 0)',
  'rgb(255, 255, 0)',
  'rgb(10, 0, 0)',
  'rgb(75, 0, 0)'
];

var cuadro=document.querySelectorAll(".cuadro");
var h1 = document.getElementById("label");
var muestra=document.querySelector("#muestraRGB");
var btnreset=document.querySelector("#btnReset");
var btnFacil=document.querySelector("#btnFacil");
var btnDificil=document.querySelector("#btnDificil");
var mensaje=document.querySelector("#mensaje");
colores=generarColores(6);
var colorSeleccionado = colores[Math.floor(Math.random()*6)];
muestra.textContent = colorSeleccionado;

var numCuadros = 6;

btnFacil.addEventListener("click", function(){
    btnDificil.classList.remove("seleccionado");
    btnFacil.classList.add("seleccionado");
    numCuadros = 3;
    colores = generarColores(numCuadros);
    colorSeleccionado = colores[Math.floor(Math.random()*numCuadros)];
    for (var i = 0; i < 3; i++) {
        cuadro[i].style.display= "none";
    }
});

btnDificil.addEventListener("click", function(){
    btnFacil.classList.remove("seleccionado");
    btnDificil.classList.add("seleccionado");
    numCuadros = 6;
    colores = generarColores(numCuadros);
    colorSeleccionado = colores[Math.floor(Math.random()*numCuadros)];
    for (var i = 0; i < 6; i++) {
        cuadro[i].style.display= "block";
    }
});

btnreset.addEventListener("click", function()
{
    colores=generarColores(numCuadros);
    colorSeleccionado=colores[Math.floor(Math.random()*numCuadros)];
    muestra.textContent=colorSeleccionado;
    for(var i= 0; i<cuadro.length; i++){
        cuadro[i].style.backgroundColor=colores[i];
    }
    h1.style.backgroundColor = "Steelblue";

});


for (var  i=0 ; i< cuadro.length;i++){
 
    cuadro[i].style.backgroundColor = colores[i];

    cuadro[i].addEventListener("click",function(){
       var seleccion = this.style.backgroundColor;

        if (colorSeleccionado == seleccion) {
           
           colorGanador(colorSeleccionado);
        } else {
        
            //alert("No seleccionado");
            this.style.backgroundColor = "#232323";
        }


    });
    
   
}

function colorGanador(color) {
    h1.style.backgroundColor = color;
    for (let i = 0; i < cuadro.length; i++) {

        cuadro[i].style.backgroundColor = color; 
        
    }
}

function generarColores(num){
    var arr=[];
    for(var i=0;i<num;i++){

    arr.push(colorgenerado());

    }
return arr;
}


function colorgenerado(){
var r=Math.floor(Math.random()*256);
var g=Math.floor(Math.random()*256);
var b=Math.floor(Math.random()*256);

return "rgb("+r+", "+g+", "+b+")";
}

